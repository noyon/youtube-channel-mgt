**Youtube Channel Management Portal**

*Just add your youtube channel ID, It will automatically scraping out your 
all youtube public videos. It will find out how many likes, comments, 
views are there. It will automatically pull that info to give you a 
better idea of how your videos are performing. You can add as many 
youtube channels as you like. But the only requirement is you have to 
be a registered admin user.*
---

# Requirements
1. Python Version >= 3.6.5
2. Django>=3.0.4
3. requests
4. mysqlclient (Optional)


## Installation Guide Using Virtual Environment
1. Install python 3.6.5 or latest version
2. open terminal or cmd
3. Follow the commends
```sh
$ virtualenv env
$ cd env
$ source bin/activate (in linux)
$ cd Scripts && activate && cd .. (in windows)
$ mkdir src
$ cd src
$ git clone https://noyon@bitbucket.org/noyon/youtube-channel-mgt.git
$ cd youtube-channel-mgt
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py makemigrations channel
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```
4. Then browse: 127.0.0.1:8000
6. All done


## Installation Guide Using Dockerfile
1. Follow the commends
```sh
$ git clone https://github.com/noyonict/Image-Matching.git
$ cd Image-Matching
$ docker-compose build
$ docker-compose up
```
2. Then browse: 127.0.0.1:8800
4. All done

## Installation Guide Using Dockerfile
1. Follow the commends
```sh
$ git clone https://github.com/noyonict/Image-Matching.git
$ cd Image-Matching
$ docker-compose build
$ docker-compose up
```
2. Then browse: 127.0.0.1:8800
4. All done

## Add MySQL Database (Optional)
#####1. If you don't want to use sqlite database. Use MySQL 
```sh
$ sudo apt-get install mysql-server
$ systemctl status mysql.service
You will see output that looks similar to this:
mysql.service active
● mysql.service - MySQL Community Server
   Loaded: loaded (/lib/systemd/system/mysql.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2017-12-29 11:59:33 UTC; 1min 44s ago
 Main PID: 26525 (mysqld)
   CGroup: /system.slice/mysql.service
        └─26525 /usr/sbin/mysqld

Dec 29 11:59:32 ubuntu-512mb-nyc3-create-app-and-mysql systemd[1]: Starting MySQL Community Server...
Dec 29 11:59:33 ubuntu-512mb-nyc3-create-app-and-mysql systemd[1]: Started MySQL Community Server.

$ mysql -u db_user -p
Then you will see output that asks you for this db_user’s password:
Output
Enter password:

Once you enter your password correctly, you will see the following output:
Output
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 6
Server version: 5.7.20-0ubuntu0.16.04.1 (Ubuntu)

Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

$ CREATE DATABASE youtube;
$ SHOW DATABASES;
It will show your youtube database

```
#####2. Add the MySQL Database Connection to your Application.
Navigate to the settings.py file and replace the current DATABASES lines with the following. We will configure your database dictionary so that it knows to use MySQL as your database backend and from what file to read your database connection credentials:
settings.py
...
###### Database
###### https://docs.djangoproject.com/en/2.0/ref/settings/#databases
settings.py
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dbname',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'"
        },
    }
}
```
...
#####3. python manage.py makemigrations
#####4. Python manage.py migrate

## Technology Used
- **os** - Linux
- **Programming Language** - Python3
- **Framework** - Django
- **Database** - SQLite3, mySQL
- **Web Server** - Ngnix
---

## How to use the application

*When you everything setup correctly. Then browse. 127.0.0.1:8000*

1. You will get login page. Give your superuser username and password
![](images/login-page.PNG)
2. You can see the admin page. You need to just add a channel by clicking the 
**+add** section of the **youtube Channel List**
![](images/admin-panel%201.PNG)
3. Here you just need to add **Channel id**. You will find chanel id from 
youtube channel home page URL
![](images/youtube-channel-id.PNG)
4. Just add **Channel id** like that. Then click save.
![](images/channel-id-add-option.PNG)
5. System will automatically bring all of the channel information.
![](images/create-channel.PNG)
6. It will also bring all the video list from the channel.
![](images/video-created.PNG)
7. If you want to update video information then check the **IS Updated info**
![](images/update-video-info.PNG)
8. If will bring more info and update the video info
![](images/video-info-updated.PNG)


That's all. Use the system and you will automatically comfortable with it. It has no view.
If you want you can add the view and html template to better visualization.

---
