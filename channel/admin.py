from django.contrib import admin
from .models import YoutubeChannel, ChannelVideo, Setting


class YoutubeChannelModelAdmin(admin.ModelAdmin):
    list_display = ['channel_id', 'channel_name', 'how_many_videos', 'view_count', 'subscriber_count', 'is_valid_channel', 'is_published', 'update_at']
    list_display_links = ['channel_name', 'channel_id', 'how_many_videos', 'update_at']
    list_editable = ['is_published', ]
    list_filter = ['is_valid_channel', 'is_published', 'create_at', 'update_at']
    search_fields = ['channel_name', 'channel_id']

    class Meta:
        model = YoutubeChannel


admin.site.register(YoutubeChannel, YoutubeChannelModelAdmin)


class ChannelVideoModelAdmin(admin.ModelAdmin):
    list_display = ['video_title', 'view_count', 'like_count', 'dislike_count', 'comment_count', 'is_update_info']
    list_display_links = ['video_title', 'view_count', 'like_count', 'dislike_count', 'comment_count']
    list_editable = ['is_update_info', ]
    list_filter = ['youtube_channel', 'video_performance_rating', 'create_at', 'update_at']
    search_fields = ['video_title', 'description', 'upload_status', 'failure_reason', 'rejection_reason']

    class Meta:
        model = ChannelVideo


admin.site.register(ChannelVideo, ChannelVideoModelAdmin)


class SettingModelAdmin(admin.ModelAdmin):
    list_display = ['google_api_key', 'inquiry_interval', 'create_at', 'update_at']
    list_display_links = ['create_at', 'update_at']
    list_editable = ['google_api_key', 'inquiry_interval']

    class Meta:
        model = Setting


admin.site.register(Setting, SettingModelAdmin)
