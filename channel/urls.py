from django.urls import path
from . import views

app_name = 'channel'

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create_video, name='create_video'),
    path('update', views.update_video, name='update_video'),
]


