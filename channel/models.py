from django.db import models
from django.db.models.signals import post_save, pre_save
import requests


class YoutubeChannel(models.Model):
    channel_id = models.CharField(max_length=255, help_text='Id of your youtube channel', unique=True)
    channel_name = models.CharField(max_length=500, help_text='Name of you youtube channel.', blank=True, null=True)
    how_many_videos = models.PositiveIntegerField(blank=True, null=True,
                                                  help_text='Number of videos of your youtube channel.')
    view_count = models.PositiveIntegerField(blank=True, null=True,
                                             help_text='Total number of view of your youtube channel')
    subscriber_count = models.PositiveIntegerField(blank=True, null=True,
                                                   help_text='Total number of subscriber of your youtube channel')
    upload_playlist_id = models.CharField(max_length=255, blank=True, null=True,
                                          help_text='Your channel upload playlist id')
    channel_create_at = models.CharField(max_length=300, blank=True, null=True,
                                         help_text='Your channels creation date.')
    channel_description = models.TextField(blank=True, null=True, help_text='Description of your youtube channel.')
    is_valid_channel = models.BooleanField(default=True,
                                           help_text="Don't edit it. It will automatically updated.")
    is_published = models.BooleanField(default=True,
                                       help_text="Unchecked, If you don't want to publish this channel info.")
    create_at = models.DateTimeField(auto_now_add=True, help_text='When you created this channel info.')
    update_at = models.DateTimeField(auto_now=True, help_text='Last update time of this channel.')

    def save(self, *args, **kwargs):
        api_link = 'https://6tw4yburaf.execute-api.ap-south-1.amazonaws.com/dev?channel_id={}'.format(self.channel_id)
        snippet = requests.get(api_link)
        try:
            self.channel_name, self.view_count, self.how_many_videos, self.subscriber_count, self.upload_playlist_id, self.channel_description, self.channel_create_at = snippet.json()
            self.is_valid_channel = True
        except Exception as e:
            self.is_valid_channel = False
            self.is_published = False
            print(e)
        super().save(*args, **kwargs)

    def __str__(self):
        if self.channel_name:
            return self.channel_name
        else:
            return self.channel_id

    class Meta:
        db_table = 'youtube_channel'
        ordering = ('-create_at', )
        verbose_name = 'Youtube Channel'
        verbose_name_plural = 'Youtube Channel List'


class ChannelVideo(models.Model):
    video_title = models.CharField(max_length=500, help_text='title of the youtube video.')
    youtube_channel = models.ForeignKey(YoutubeChannel, on_delete=models.CASCADE,
                                        related_name='youtube_channel', help_text='Youtube channel name of the video.')
    video_id = models.CharField(max_length=200, blank=True, null=True, unique=True, help_text='id of the youtube video.')
    view_count = models.PositiveIntegerField(blank=True, null=True, help_text='Number of views of the video.')
    like_count = models.PositiveIntegerField(blank=True, null=True, help_text='Number of views of the video.')
    dislike_count = models.PositiveIntegerField(blank=True, null=True, help_text='Number of views of the video.')
    comment_count = models.PositiveIntegerField(blank=True, null=True, help_text='Number of views of the video.')
    duration = models.CharField(max_length=50, blank=True, null=True, help_text='Duration of the video.')
    tags = models.CharField(max_length=500, blank=True, null=True, help_text='Tags of the video.')
    description = models.TextField(blank=True, null=True, help_text='Description of the video.')
    upload_status = models.CharField(max_length=150, blank=True, null=True, help_text='uploadStatus of the video.')
    public_stats_viewable = models.BooleanField(blank=True, null=True)
    video_performance_rating = models.IntegerField(blank=True, null=True,
                                                   help_text='Video views divided by channels all videos views median')
    is_update_info = models.BooleanField(default=False,
                                         help_text="Check if you want to update the information.")
    is_published = models.BooleanField(default=True,
                                       help_text="Unchecked, If you don't want to publish this channel info.")
    create_at = models.DateTimeField(auto_now_add=True, help_text='When you created this video info.')
    update_at = models.DateTimeField(auto_now=True, help_text='Last update time of this video.')

    def save(self, *args, **kwargs):
        print(self.video_title)
        super().save(*args, **kwargs)
        # pass

    def __str__(self):
        return self.video_title

    class Meta:
        db_table = 'channel_video'
        ordering = ('-create_at', )
        verbose_name = 'Channel Video'
        verbose_name_plural = 'Channel Video List'


class Setting(models.Model):
    google_api_key = models.CharField(max_length=200, default='AIzaSyCzBTFNRN5VE_qKSAsGIhJt-tXLBhYcx8U',
                                      help_text='your google developer api key for youtube v3')
    inquiry_interval = models.PositiveIntegerField(default=5,
                                                   help_text='How many time interval your video info will retrieve.')

    create_at = models.DateTimeField(auto_now_add=True, help_text='When you created this video info.')
    update_at = models.DateTimeField(auto_now=True, help_text='Last update time of this video.')

    def save(self, *args, **kwargs):
        print(self.google_api_key)
        super().save(*args, **kwargs)
        # pass

    def __str__(self):
        return self.google_api_key

    class Meta:
        db_table = 'settings'
        ordering = ('-create_at',)
        verbose_name = 'Setting'
        verbose_name_plural = 'Settings'


def get_channel_video_info(sender, instance, created, **kwargs):
    print('Signal is working', instance.upload_playlist_id)
    if instance.upload_playlist_id and created:
        videos = requests.get('https://6tw4yburaf.execute-api.ap-south-1.amazonaws.com/dev/videos?playlist_id={}'.format(instance.upload_playlist_id))
        for video in videos.json():
            title = str(video['snippet']['title'])
            description = str(video['snippet']['description'])
            video_id = str(video['snippet']['resourceId']['videoId'])
            try:
                ChannelVideo.objects.create(video_title=title, video_id=video_id, description=description,
                                            youtube_channel=instance)
                print('Created ', title)
            except Exception as e:
                print(e)
    else:
        print(instance, 'Is not new instance or Has no playlist id.')


post_save.connect(get_channel_video_info, YoutubeChannel)


def update_video_info(sender, instance, created, **kwargs):
    print('Video id:', instance.video_id)
    if instance.video_id and instance.is_update_info and not created:
        video_info = requests.get('https://6tw4yburaf.execute-api.ap-south-1.amazonaws.com/dev/video-details?video_id={}'.format(instance.video_id))
        instance.view_count, instance.like_count, instance.dislike_count, instance.comment_count, instance.duration, instance.upload_status, instance.public_stats_viewable, instance.tags = video_info.json()
        instance.is_update_info = False
        instance.save()
    else:
        print(instance, 'updated successfully!')


post_save.connect(update_video_info, ChannelVideo)
